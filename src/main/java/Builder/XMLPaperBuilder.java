package Builder;

import Paper.Paper;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Class for building xml file with paper objects.
 */
public class XMLPaperBuilder {
    /**
     * List of paper objects, which will be added in xml file.
     */
    List<Paper> paper;

    public XMLPaperBuilder() {}

    public XMLPaperBuilder(List<Paper> paper) {
        this.paper = paper;
    }

    public void setPaper(List<Paper> paper) {
        this.paper = paper;
    }

    /**
     * Create xml document
     * @param filename - path
     */
    public void createXmlDocument(String filename) {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
                .newInstance();
        DocumentBuilder documentBuilder = null;
        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        // forming a document tree
        Document document = documentBuilder.newDocument();
        Element rootElement = document.createElement("Papers");
        document.appendChild(rootElement);
        for (var x: paper) {
            Element subRootElement = document.createElement("Paper");

            Element elementID = document.createElement("id");
            elementID.appendChild(document.createTextNode(x.getId()));

            Element elementName = document.createElement("name");
            elementName.appendChild(document.createTextNode(x.getName()));

            Element elementMonthly = document.createElement("monthly");
            elementMonthly.appendChild(document.createTextNode(x.isMonthly() ? "true" : "false"));

            Element charsElement = document.createElement("Chars");

            Element elementType = document.createElement("type");
            elementType.appendChild(document.createTextNode(x.getChars().getType().name()));

            Element elementSize = document.createElement("size");
            elementSize.appendChild(document.createTextNode(new Integer(x.getChars().getSize()).toString()));

            Element elementColor = document.createElement("color");
            elementColor.appendChild(document.createTextNode(x.getChars().isColor() ? "true" : "false"));

            Element elementGlossy = document.createElement("glossy");
            elementGlossy.appendChild(document.createTextNode(x.getChars().isGlossy() ? "true" : "false"));

            charsElement.appendChild(elementType);
            charsElement.appendChild(elementSize);
            charsElement.appendChild(elementColor);
            charsElement.appendChild(elementGlossy);

            subRootElement.appendChild(elementID);
            subRootElement.appendChild(elementName);
            subRootElement.appendChild(elementMonthly);
            subRootElement.appendChild(charsElement);

            rootElement.appendChild(subRootElement);
        }
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        try {
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(new FileWriter(filename));
            transformer.transform(source, result);
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException | IOException e) {
            e.printStackTrace();
        }
    }
}
