import Paper.*;
import Parsers.*;

import Builder.XMLPaperBuilder;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Main {
    public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException, XMLStreamException {
        /*List<Paper> list = new ArrayList<>();

        list.add(new Paper("115", "Pravda", true,
                new Chars(40, PaperType.newspaper, false, false)));

        list.add(new Paper("955", "New York Times", true,
                new Chars(31, PaperType.magazine, true, true)));

        list.add(new Paper("210", "Booklet about all", false,
                new Chars(64, PaperType.booklet, true, true)));

        XMLPaperBuilder builder = new XMLPaperBuilder(list);
        builder.createXmlDocument("src/other/papers.xml");*/

        PaperParser planeParser = new PaperParser();

        System.out.println("SAX parser:");
        System.out.println(planeParser.parse("src/other/papers.xml",
                "src/other/Paper.xsd", "sax"));

        System.out.println("\nStAX parser:");
        System.out.println(planeParser.parse("src/other/papers.xml",
                "src/other/Paper.xsd", "stax"));

        System.out.println("\nDOM parser:");
        System.out.println(planeParser.parse("src/other/papers.xml",
                "src/other/Paper.xsd", "dom"));
    }
}
