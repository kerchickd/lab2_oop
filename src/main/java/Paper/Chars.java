package Paper;

import java.util.Objects;

/**
 * Chars class -  additional param of paper
 */
public class Chars {
    /**
     * Paper size
     */
    private int size;
    /**
     * Type of paper
     */
    private PaperType type;
    /**
     * Color paper or not
     */
    private boolean color;
    /**
     * Glossy paper or not
     */
    private boolean glossy;

    public Chars() {}

    public Chars(int size, PaperType type, boolean color, boolean glossy) {
        this.size = size;
        this.type = type;
        this.color = color;
        this.glossy = glossy;
    }

    @Override
    public String toString() {
        return "Chars{" +
                "size=" + size +
                ", type=" + type +
                ", color=" + (color ? "yes" : "no") +
                ", glossy=" + (glossy ? "yes" : "no") +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Chars chars = (Chars) o;
        return size == chars.size && color == chars.color && glossy == chars.glossy && type == chars.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(size, type, color, glossy);
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public PaperType getType() {
        return type;
    }

    public void setType(PaperType type) {
        this.type = type;
    }

    public boolean isColor() {
        return color;
    }

    public void setColor(boolean color) {
        this.color = color;
    }

    public boolean isGlossy() {
        return glossy;
    }

    public void setGlossy(boolean glossy) {
        this.glossy = glossy;
    }
}
