package Paper;

import java.util.Objects;

/**
 * Paper class
 */
public class Paper {
    /**
     * id of paper
     */
    private String id;
    /**
     * Name of paper
     */
    private String name;
    /**
     * Monthly paper or not
     */
    private boolean monthly;
    /**
     * Additional param of paper
     */
    private Chars chars;

    public Paper() {
        chars = new Chars();
    }

    public Paper(String id, String name, boolean monthly, Chars chars) {
        this.id = id;
        this.name = name;
        this.monthly = monthly;
        this.chars = chars;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isMonthly() {
        return monthly;
    }

    public void setMonthly(boolean monthly) {
        this.monthly = monthly;
    }

    public Chars getChars() {
        return chars;
    }

    public void setChars(Chars chars) {
        this.chars = chars;
    }

    @Override
    public String toString() {
        return "Paper{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", monthly=" + monthly +
                ", chars=" + chars +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Paper paper = (Paper) o;
        return monthly == paper.monthly && id.equals(paper.id) && name.equals(paper.name) && chars.equals(paper.chars);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, monthly, chars);
    }
}
