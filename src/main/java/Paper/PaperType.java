package Paper;

/**
 * Available types of paper
 */
public enum PaperType {
    newspaper, magazine, booklet;
}
