package Paper;

import java.util.Collections;
import java.util.List;

/**
 * List of papers
 */
public class Papers {
    /**
     * List of papers
     */
    private List<Paper> paperList;

    public Papers(List<Paper> paperList) {
        this.paperList = paperList;
    }

    public List<Paper> getList(){
        return paperList;
    }


    @Override
    public String toString() {
        return "Papers{" +
                "paperList=" + paperList +
                '}';
    }
}
