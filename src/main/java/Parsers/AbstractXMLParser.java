package Parsers;

/**
 * Abstract class of XML parsers(DOM, StAX, SAX)
 */
public abstract class AbstractXMLParser {
    /**
     * Method for parsing
     * @param xmlPath - path of XML document
     */
    public abstract void parse(String xmlPath);
}
