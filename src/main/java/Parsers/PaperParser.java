package Parsers;

import Paper.Papers;
import Parsers.DOMParser.DOMParser;
import Parsers.SAXParser.SAXPaperParser;
import Parsers.SAXParser.SAXPaperParser;
import Parsers.StAXParser.StAXParser;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;

/**
 * Paper parser
 */
public class PaperParser {
    /**
     * Abstract XML parser
     */
    private AbstractXMLParser parser;
    /**
     * Result of parsing
     */
    private Papers result;
    /**
     * XML handler
     */
    private XMLHandler handler;

    /**
     * Method for parsing
     * @param xmlPath - path of XML document
     * @param xsdPath - path of XSD document
     * @param parserName - name of parsing method(DOM, SAX, STAX)
     * @return result of parsing
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @throws XMLStreamException
     */
    public Papers parse(String xmlPath, String xsdPath, String parserName) throws ParserConfigurationException, SAXException, IOException, XMLStreamException {

        if(XMLValidator.validateXML(xmlPath,xsdPath)) {

            switch (parserName.toUpperCase()) {
                case "SAX": {
                    handler = new XMLHandler();
                    parser = new SAXPaperParser(handler);
                    parser.parse(xmlPath);
                    result = new Papers(handler.getPapers());
                    break;
                }
                case "DOM": {
                    handler = new XMLHandler();
                    parser = new DOMParser(handler);
                    parser.parse(xmlPath);
                    result = new Papers(handler.getPapers());
                    break;
                }
                case "STAX": {
                    handler = new XMLHandler();
                    parser = new StAXParser(handler);
                    parser.parse(xmlPath);
                    result = new Papers(handler.getPapers());
                    break;
                }
                default:
                    break;
            }
        }
        return result;
    }
}
