package Parsers.SAXParser;

import Parsers.XMLHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * SAX Handler
 */
public class SAXHandler extends DefaultHandler{
    /**
     * XML handler
     */
    private XMLHandler handler;
    /**
     * Data
     */
    private StringBuilder data;

    public SAXHandler(XMLHandler handler){
        this.handler = handler;
        data = null;
    }

    /**
     * Start element
     * @param uri
     * @param localName
     * @param qName
     * @param attributes
     * @throws SAXException
     */
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException{
        data = new StringBuilder();
    }

    /**
     * End element
     * @param uri
     * @param localName
     * @param qName
     * @throws SAXException
     */
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if(qName.equalsIgnoreCase(handler.name))
            handler.endTag(qName);
        handler.setTag(qName, data.toString());
        data = new StringBuilder();
    }

    /**
     * Characters
     * @param ch
     * @param start
     * @param length
     * @throws SAXException
     */
    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
        data.append(new String(ch, start, length));
    }
}
