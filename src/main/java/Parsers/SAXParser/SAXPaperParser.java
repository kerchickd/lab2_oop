package Parsers.SAXParser;

import Parsers.AbstractXMLParser;
import Parsers.XMLHandler;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;

/**
 * SAX parser for paper class
 */
public class SAXPaperParser extends AbstractXMLParser {
    /**
     * XML handler
     */
    private XMLHandler handler;
    public SAXPaperParser(XMLHandler handler){
        this.handler = handler;
    }

    /**
     * Method for parsing
     * @param xmlPath - path of XML document
     */
    @Override
    public void parse(String xmlPath) throws IllegalArgumentException {

        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        try {
            SAXParser saxParser = saxParserFactory.newSAXParser();
            SAXHandler saxHandler = new SAXHandler(handler);
            saxParser.parse(new File(xmlPath), saxHandler);

        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Error: " + e.getMessage());
        }
    }
}
