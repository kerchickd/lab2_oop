package Parsers;

import Paper.*;

import java.util.ArrayList;
import java.util.List;

/**
 * XML handler class
 */
public class XMLHandler {
    /**
     * Paper
     */
    private Paper paper;
    /**
     * List of paper
     */
    private List<Paper> papers;
    /**
     * Const string
     */
    public final String name = "Paper";

    public XMLHandler() {
        paper = new Paper();
        papers = new ArrayList<>();
    }

    public Paper getPaper(){
        return paper;
    }

    public List<Paper> getPapers(){
        return papers;
    }

    /**
     * Set tag for handler
     * @param element
     * @param value
     */
    public void setTag( String element, String value){
        switch (element.toLowerCase()){
            case "paper":
                paper = new Paper();
            case "id":
                paper.setId(value);
                break;
            case "name":
                paper.setName(value);
                break;
            case "monthly":
                paper.setMonthly(Boolean.parseBoolean(value));
                break;
            case "type":
                paper.getChars().setType(PaperType.valueOf(value));
                break;
            case "size":
                paper.getChars().setSize(Integer.parseInt(value));
                break;
            case "color":
                paper.getChars().setColor(Boolean.parseBoolean(value));
                break;
            case "glossy":
                paper.getChars().setGlossy(Boolean.parseBoolean(value));
                break;
            default:
                break;
        }
    }

    /**
     * End tag of handler
     * @param element
     */
    public void endTag(String element){
        if(element.equalsIgnoreCase("paper")) {
            papers.add(paper);
            paper = new Paper();
        }
    }
}
