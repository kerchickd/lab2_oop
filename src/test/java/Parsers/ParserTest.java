package Parsers;

import Paper.*;
import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class ParserTest {
    PaperParser paperParser;
    Paper paper1;
    Paper paper2;

    ParserTest(){
        paperParser = new PaperParser();
        paper1 = new Paper();
        paper2 = new Paper();

        paper1.setId("1");
        paper1.setName("New magazine");
        paper1.setMonthly(false);
        paper1.setChars(new Chars(150, PaperType.magazine, false, true));

        paper2.setId("2");
        paper2.setName("Booklet new");
        paper2.setMonthly(true);
        paper2.setChars(new Chars(1500, PaperType.booklet, true, true));

    }

    @Test
    void DOMParserTest() throws ParserConfigurationException, XMLStreamException, SAXException, IOException {
        Papers papers = paperParser.parse("src/other/test/check.xml",
                "src/other/Paper.xsd", "dom");
        assertEquals(papers.getList().get(0), paper1);
        assertEquals(papers.getList().get(1), paper2);
    }

    @Test
    void StAXParserTest() throws ParserConfigurationException, XMLStreamException, SAXException, IOException {
        Papers papers = paperParser.parse("src/other/test/check.xml",
                "src/other/Paper.xsd", "dom");
        assertEquals(papers.getList().get(0), paper1);
        assertEquals(papers.getList().get(1), paper2);
    }

    @Test
    void SAXParserTest() throws ParserConfigurationException, XMLStreamException, SAXException, IOException {
        Papers papers = paperParser.parse("src/other/test/check.xml",
                "src/other/Paper.xsd", "dom");
        assertEquals(papers.getList().get(0), paper1);
        assertEquals(papers.getList().get(1), paper2);
    }

    @Test
    void XMLHandlerTest(){
        XMLHandler handler = new XMLHandler();
        handler.setTag("id", "1");
        handler.setTag("name", "New magazine");
        handler.setTag("monthly", "false");
        handler.setTag("size", "150");
        handler.setTag("type","magazine");
        handler.setTag("color", "false");
        handler.setTag("glossy", "true");

        assertEquals(paper1, handler.getPaper());

        XMLHandler handler1 = new XMLHandler();
        handler1.setTag("id", "2");
        handler1.setTag("name", "Booklet new");
        handler1.setTag("monthly", "true");
        handler1.setTag("size", "1500");
        handler1.setTag("type","booklet");
        handler1.setTag("color", "true");
        handler1.setTag("glossy", "true");

        assertEquals(paper2, handler1.getPaper());
    }

}
