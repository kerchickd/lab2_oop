package Parsers;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class XMLValidatorTest {
    @Test
    void validXMLValidation(){
        assertTrue(XMLValidator.validateXML("src/other/test/right.xml",
                "src/other/Paper.xsd"));
    }

    @Test
    void invalidXMLValidation(){
        assertFalse(XMLValidator.validateXML("src/other/test/wrong.xml",
                "src/other/Paper.xsd"));
        assertFalse(XMLValidator.validateXML("src/other/test/wrong1.xml",
                "src/other/Paper.xsd"));
        assertFalse(XMLValidator.validateXML("src/other/test/wrong2.xml",
                "src/other/Paper.xsd"));
    }
}